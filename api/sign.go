package api

import (
	"digital_sign/action"
	"digital_sign/model"
	"digital_sign/model/obj"
	"encoding/base64"
	"encoding/json"
	"gitlab.com/thuocsi.vn-sdk/go-sdk/sdk"
)

type RequestBody struct {
	Text string `json:"text,omitempty"`
}

func Sign(req sdk.APIRequest, res sdk.APIResponder) error {
	var textInput *RequestBody
	err := req.GetContent(&textInput)
	if err != nil {
		response := obj.WithInvalidInput(err)
		return res.Respond(response)
	}
	bytes, err := base64.StdEncoding.DecodeString(textInput.Text)
	if err != nil {
		response := obj.WithInvalidInput(err)
		return res.Respond(response)
	}
	var input *model.Signature
	err = json.Unmarshal(bytes, &input)
	if err != nil {
		response := obj.WithInvalidInput(err)
		return res.Respond(response)
	}
	input.SignatureCode = input.Password[len(input.Password)-10:]
	input.Password = input.Password[:len(input.Password)-10]
	return res.Respond(action.Sign(*input))
}

func VerifySignature(req sdk.APIRequest, res sdk.APIResponder) error {
	var input *model.SignatureBody
	err := req.GetContent(&input)
	if err != nil {
		return res.Respond(obj.WithInvalidInput(err))
	}
	return res.Respond(action.Verify(*input))
}
