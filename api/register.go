package api

import (
	"digital_sign/action"
	"digital_sign/model"
	"gitlab.com/thuocsi.vn-sdk/go-sdk/sdk"
	"gitlab.com/thuocsi.vn-sdk/go-sdk/sdk/common"
)

type SignRequest struct {
	Crypt string `json:"crypt"`
}

func Register(req sdk.APIRequest, res sdk.APIResponder) error {
	var input model.Signature
	req.GetContent(&input)
	if input.Username == "" {
		return res.Respond(&common.APIResponse{
			Status:  common.APIStatus.Invalid,
			Message: "Need username in request body",
		})
	}

	if input.Password == "" {
		return res.Respond(&common.APIResponse{
			Status:  common.APIStatus.Invalid,
			Message: "Need password in request body",
		})
	}
	return res.Respond(action.Register(input))
}

func RegisterPublicKey(req sdk.APIRequest, res sdk.APIResponder) error {
	var input model.Signature
	req.GetContent(&input)
	if input.Username == "" {
		return res.Respond(&common.APIResponse{
			Status:  common.APIStatus.Invalid,
			Message: "Need username in request body",
		})
	}

	if input.PublicKey == "" {
		return res.Respond(&common.APIResponse{
			Status:  common.APIStatus.Invalid,
			Message: "Need publicKey in request body",
		})
	}
	return res.Respond(action.RegisterPublicKey(input))
}
