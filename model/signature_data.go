package model

type SignatureData struct {
	Username      string      `json:"username,omitempty"`
	SignatureCode string      `json:"signatureCode,omitempty"`
	CreatedTime   int64       `json:"createdTime,omitempty"`
	Data          interface{} `json:"data,omitempty"`
}

type SignatureBody struct {
	SignatureData SignatureData `json:"signatureData,omitempty"`
	Signature     string        `json:"signature,omitempty"`
}
