package model

type SignLog struct {
	Username      string        `json:"username,omitempty" bson:"username,omitempty"`
	SignatureData SignatureData `json:"signatureData,omitempty" bson:"signature_data,omitempty"`
	SignatureCode string        `json:"signatureCode,omitempty" bson:"signature_code,omitempty"`
	Signature     string        `json:"signature,omitempty" bson:"signature,omitempty"`
}
