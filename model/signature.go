package model

import (
	"digital_sign/model/obj"
	"gitlab.com/thuocsi.vn-sdk/go-sdk/sdk/db"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
)

type Signature struct {
	SignatureID   int64      `json:"signatureID,omitempty" bson:"signature_id,omitempty"`
	SignatureCode string     `json:"signatureCode,omitempty" bson:"signature_code,omitempty"`
	Username      string     `json:"username,omitempty" bson:"username,omitempty"`
	Password      string     `json:"password,omitempty" bson:"password,omitempty"`
	PrivateKey    string     `json:"privateKey,omitempty" bson:"private_key,omitempty"`
	PublicKey     string     `json:"publicKey,omitempty" bson:"public_key,omitempty"`
	IsActive      *bool      `json:"isActive,omitempty" bson:"Is_active,omitempty"`
	LastUse       *time.Time `json:"lastUse,omitempty" bson:"last_use,omitempty"`

	Crypt string `json:"crypt,omitempty" bson:"-"`
}

var DBSignature = &db.Instance{
	ColName:        "signature",
	TemplateObject: Signature{},
}

func InitDBSignature(database *mongo.Database) {
	DBSignature.ApplyDatabase(database)

	DBSignature.CreateIndex(bson.D{
		{"username", 1},
	}, &options.IndexOptions{
		Background: obj.WithBool(true),
	})
}
