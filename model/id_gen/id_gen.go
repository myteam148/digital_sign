package id_gen

import (
	"gitlab.com/thuocsi.vn-sdk/go-sdk/sdk/db"
	"go.mongodb.org/mongo-driver/mongo"
	"math"
)

const (
	template = "1246789HJKLXCVBQWERTYUPADFG"
)

type IDGen struct {
	ID    string `json:"id,omitempty" bson:"_id,omitempty"`
	Value uint64 `json:"value,omitempty" bson:"value,omitempty"`
}

var dbModel = &db.Instance{
	ColName:        "_id_gen",
	TemplateObject: IDGen{},
}

func InitIDGenModel(database *mongo.Database) {
	dbModel.ApplyDatabase(database)

	dbModel.Create(IDGen{
		ID:    signature,
		Value: 10000,
	})
}

func new(code string) (uint64, string) {
	result := dbModel.IncreOne(IDGen{ID: code}, "value", 1)
	val := result.Data.([]IDGen)[0]
	_code := ConvertToCode(val.Value, 10, template)
	return val.Value, _code
}

func ConvertToCode(number uint64, length int64, template string) string {
	var result = ""
	var i = int64(0)
	var ln = uint64(len(template))
	var capacity = uint64(math.Pow(float64(ln), float64(length)))
	number = number % capacity
	for i < length {
		var cur = number % ln
		if i > 0 {
			cur = (cur + uint64(result[i-1])) % ln
		}
		result = result + string(template[cur])
		number = number / ln
		i++
	}
	return result
}
