package id_gen

const (
	signature = "SIGNATURE"
)

func NewSignature() (uint64, string) {
	return new(signature)
}
