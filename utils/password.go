package utils

import (
	"encoding/base64"
	"golang.org/x/crypto/bcrypt"
)

func CheckPassword(pwB64 string, pwHash string) bool {
	newPassword, err := base64.StdEncoding.DecodeString(pwB64)
	if err != nil {
		return false
	}
	return CheckPasswordHash(string(newPassword), pwHash)
}

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}
