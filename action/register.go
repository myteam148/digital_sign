package action

import (
	"crypto/rand"
	"crypto/rsa"
	"digital_sign/model"
	"digital_sign/model/id_gen"
	"digital_sign/model/obj"
	"digital_sign/utils"
	"encoding/base64"
	"fmt"
	"gitlab.com/thuocsi.vn-sdk/go-sdk/sdk/common"
	"time"
)

func Register(input model.Signature) *common.APIResponse {
	now := time.Now()
	privateKey, _ := rsa.GenerateKey(rand.Reader, 2048)

	privateKeyBytes := utils.PrivateKeyToBytes(privateKey)
	signatureId, signatureCode := id_gen.NewSignature()

	newPassword, err := base64.StdEncoding.DecodeString(input.Password)
	if err != nil {
		return &common.APIResponse{
			Status:  common.APIStatus.Invalid,
			Message: "Can't decode password",
		}
	}

	pwHash, err := utils.HashPassword(string(newPassword))
	if err != nil {
		return &common.APIResponse{
			Status:  common.APIStatus.Invalid,
			Message: "Can't hash password",
		}
	}

	model.DBSignature.Create(model.Signature{
		SignatureCode: signatureCode,
		SignatureID:   int64(signatureId),
		Username:      input.Username,
		Password:      pwHash,
		PrivateKey:    base64.StdEncoding.EncodeToString(privateKeyBytes),
		IsActive:      obj.WithBool(true),
		LastUse:       &now,
	})

	publicKey := privateKey.PublicKey
	bytes := utils.PublicKeyToBytes(&publicKey)
	publicKeyInB64 := base64.StdEncoding.EncodeToString(bytes)
	dataString := fmt.Sprintf("%s%d%s", publicKeyInB64, now.UnixMilli(), signatureCode)

	return &common.APIResponse{
		Status:  common.APIStatus.Ok,
		Message: "Ok",
		Data:    []string{dataString},
	}
}

func RegisterPublicKey(input model.Signature) *common.APIResponse {
	now := time.Now()

	signatureId, signatureCode := id_gen.NewSignature()
	resultCreateNew := model.DBSignature.Create(model.Signature{
		SignatureID:   int64(signatureId),
		SignatureCode: signatureCode,
		Username:      input.Username,
		PublicKey:     input.PublicKey,
		LastUse:       &now,
	})

	if resultCreateNew.Status != common.APIStatus.Ok {
		return resultCreateNew
	}

	return &common.APIResponse{
		Status:  common.APIStatus.Ok,
		Message: "Ok",
		Data:    []string{signatureCode},
	}
}
