package action

import (
	"crypto"
	"crypto/rsa"
	"crypto/sha256"
	"digital_sign/model"
	"digital_sign/utils"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"gitlab.com/thuocsi.vn-sdk/go-sdk/sdk/common"
)

type Crypt struct {
	Message string `json:"message,omitempty"`
}

func Sign(input model.Signature) *common.APIResponse {
	resultCheckExistKeyPair := model.DBSignature.QueryOne(model.Signature{
		SignatureCode: input.SignatureCode,
	})
	if resultCheckExistKeyPair.Status != common.APIStatus.Ok {
		return &common.APIResponse{
			Status:  common.APIStatus.Invalid,
			Message: "Wrong signature code!",
		}
	}

	signatureData := resultCheckExistKeyPair.Data.([]model.Signature)[0]

	if !utils.CheckPassword(input.Password, signatureData.Password) {
		return &common.APIResponse{
			Status:  common.APIStatus.Invalid,
			Message: "Wrong password!",
		}
	}

	bytes, err := base64.StdEncoding.DecodeString(signatureData.PrivateKey)
	if err != nil {
		return &common.APIResponse{
			Status:  common.APIStatus.Invalid,
			Message: err.Error(),
		}
	}

	privateKey := utils.BytesToPrivateKey(bytes)

	cryptByte, err := base64.StdEncoding.DecodeString(input.Crypt)
	if err != nil {
		return &common.APIResponse{
			Status:  common.APIStatus.Invalid,
			Message: err.Error(),
		}
	}

	dataDecrypt, errFinal := utils.DecryptWithPrivateKey(string(cryptByte), *privateKey)
	fmt.Println(dataDecrypt)
	if errFinal != nil {
		return &common.APIResponse{
			Status:  common.APIStatus.Invalid,
			Message: "Invalid",
		}
	}

	return &common.APIResponse{
		Status:  common.APIStatus.Ok,
		Message: "Ok",
	}
}

func Verify(input model.SignatureBody) *common.APIResponse {
	resultGetSignature := model.DBSignature.QueryOne(model.Signature{
		SignatureCode: input.SignatureData.SignatureCode,
	})

	if resultGetSignature.Status != common.APIStatus.Ok {
		return resultGetSignature
	}
	signatureData := resultGetSignature.Data.([]model.Signature)[0]

	if signatureData.Username != input.SignatureData.Username {
		return &common.APIResponse{
			Status:  common.APIStatus.Invalid,
			Message: "username and private key mismatched",
		}
	}

	publicKeyBytes, err := base64.StdEncoding.DecodeString(signatureData.PublicKey)
	if err != nil {
		return &common.APIResponse{
			Status:  common.APIStatus.Invalid,
			Message: err.Error(),
		}
	}

	publicKey := utils.BytesToPublicKey(publicKeyBytes)

	sigB64, err2 := base64.StdEncoding.DecodeString(input.Signature)
	if err2 != nil {
		return &common.APIResponse{
			Status:  common.APIStatus.Invalid,
			Message: err2.Error(),
		}
	}

	dataB64, err3 := json.Marshal(input.SignatureData)
	if err3 != nil {
		return &common.APIResponse{
			Status:  common.APIStatus.Invalid,
			Message: err3.Error(),
		}
	}

	dataHash := sha256.Sum256(dataB64)

	//err = rsa.VerifyPSS(publicKey, crypto.SHA256, dataHash[:], sigB64, &rsa.PSSOptions{
	//	SaltLength: 32,
	//	Hash:       crypto.SHA256,
	//})

	err = rsa.VerifyPKCS1v15(publicKey, crypto.SHA256, dataHash[:], sigB64)

	if err != nil {
		return &common.APIResponse{
			Status:  common.APIStatus.Invalid,
			Message: err.Error(),
		}
	}

	return &common.APIResponse{
		Status:  common.APIStatus.Ok,
		Message: "Ok",
	}
}
