package main

import (
	"digital_sign/api"
	"digital_sign/model"
	"digital_sign/model/id_gen"
	"gitlab.com/thuocsi.vn-sdk/go-sdk/sdk"
	"gitlab.com/thuocsi.vn-sdk/go-sdk/sdk/common"
	"gitlab.com/thuocsi.vn-sdk/go-sdk/sdk/db"
	"go.mongodb.org/mongo-driver/mongo"
)

var app *sdk.App

func main() {

	app = sdk.NewApp("Digital sign project hello 1")

	app.SetupDBClient(db.Configuration{
		Address:     "mongodb://127.0.0.1:27017",
		Username:    "admin",
		AuthDB:      "admin",
		Password:    "admin",
		DBName:      "admin",
		DoWriteTest: false,
	}, func(database *mongo.Database) error {
		model.InitDBSignature(database)
		id_gen.InitIDGenModel(database)
		return nil
	})

	var server, _ = app.SetupAPIServer("HTTP")

	server.SetHandler(common.APIMethod.POST, "/register", api.Register)
	server.SetHandler(common.APIMethod.POST, "/sign", api.Sign)

	server.SetHandler(common.APIMethod.POST, "/register-signature", api.RegisterPublicKey)
	server.SetHandler(common.APIMethod.POST, "/verify-signature", api.VerifySignature)

	server.Expose(80)
	app.Launch()
}
